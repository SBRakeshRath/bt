<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Html  </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
include("../../../fonts/fonts.html")
?>
    <link rel="stylesheet" href="../../styles.css/html.css">
    <link rel="stylesheet" href="../../../assets/allnav/html/htmlnav.css">
    <link rel="stylesheet" href="">
    <?php
include("../../../favicon/favicon.html")
?>

</head>

<body>
<?php
include("../../../assets/allnav/html/htmlnav.php")
?>
<?php
include("../../../extra/html/belownav/belownav.php")
?>

 <div class="content" id="content">
 <?php
include("../../../assets/topics column/html topics/htmltopics.php")
?>




        <div class="about_topic" id="about_topic">
        <?php
include("../../../extra/html/aboveheading/aboveheading.php")
?>
            <p class="page_heading"></p>

           
            

































<?php
include("../../../extra/html/abovebutton/abovebutton.php")
?>

            <div class="pre-nex-buttons" id="pre-next-buttons">
                <a href=""> <button class="previous buttons" id="previous">PREVIOUS</button></a>
                <a href=""><button class="next buttons" id="next">NEXT</button></a>
            </div>


        </div>

        <?php
include("../../../extra/html/right side/rightside.php")
?>
        



    </div>
    <?php
include("../../../extra/html/abovefooter/abovefooter.php")
?>
    <?php
include("../../../assets/allfooter/html/htmlfooter.php")
?>


        <script src="../../javascript/html.js" async defer></script>
        <script src="" async defer></script>
</body>

</html>